#ifndef TILEGUI_H
#define TILEGUI_H

#include "..\..\DAO_Cpp\Block.h"
#include "TGUI\TGUI.hpp"
#include "TileMap.h"

class TileGui {
public:
	TileGui(Block* p_block, sf::RenderWindow& window, sf::Vector2i position, const std::string& name = "Unknown");
	void build();
	void update();
	void event(sf::Event event);
	void draw();
	void setMarginInElem(sf::Vector2f margin);
	void uncheckArbre();
	bool hasPurpose();
	sf::FloatRect getGlobalBound();
	~TileGui();
private:
	Block* p_block;

	sf::RenderWindow* window;
	tgui::Gui gui;

	tgui::Button::Ptr buttonQuit;

	tgui::CheckBox::Ptr checkBoxMur;
	tgui::CheckBox::Ptr checkBoxMurInverse;
	tgui::CheckBox::Ptr checkBoxMurMobile;
	tgui::CheckBox::Ptr checkBoxArbre;
	
	tgui::Label::Ptr labelName;
	tgui::Label::Ptr labelMur;
	tgui::Label::Ptr labelMurInverse;
	tgui::Label::Ptr labelMurMobile;
	tgui::Label::Ptr labelArbre;

	sf::RectangleShape background;
	
	sf::Vector2f margin;
	sf::Vector2f position;

	std::string name;

};

#endif