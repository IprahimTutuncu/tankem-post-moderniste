#include <SFML\Graphics.hpp>
#include <array>

class Arbres;

class Arbre : public sf::Drawable, sf::Transformable {
public:
	Arbre();
	Arbre(sf::Texture* texture, sf::Vector2i ID, float tileSize);
	void build();

private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	float size;

	sf::Texture* m_texture;
	sf::VertexArray m_vertices;

	sf::Vector2i ID;
	bool onScreen;

	friend Arbres;
};

class Arbres: public sf::Drawable, sf::Transformable{
public:
	Arbres(const std::string & fileLocation, float tileSize, sf::Vector2u gridSize);
	void addArbre(unsigned int i, unsigned int j);
	void removeArbre(int i, int j);

	void setTileSize(float tileSize);
	void setGridSize(sf::Vector2u gridSize);
	void setArbreSize(float tileSize);

private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	float tileSize;
	sf::Vector2u gridSize;
	sf::Texture m_texture;

	std::string location;
	Arbre m_sprite_arbres[12][12];
};

