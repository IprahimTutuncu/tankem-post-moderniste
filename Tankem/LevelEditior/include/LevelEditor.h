#ifndef __LEVEL_EDITOR__
#define __LEVEL_EDITOR__

#include <SFML/Graphics.hpp>
#include <TGUI\TGUI.hpp>

#include "..\..\LevelEditior\include\TileMap.h"
#include "..\..\LevelEditior\include\TileGui.h"
#include "..\..\LevelEditior\include\Alert.h"
#include <memory>

#include <iostream>


class LevelEditor {
	public:
		LevelEditor();
		~LevelEditor() = default;

		void run();
	private:
		void event();
		void update();
		void draw();
		const unsigned int SCREEN_WIDTH{ 1280 };
		const unsigned int SCREEN_HEIGHT{ 720  };

		sf::RenderWindow window;
		tgui::Gui gui;
		TileMap tilemap;

		std::unique_ptr<TileGui> p_tileGui;
		Alerts alertManager;
		sf::Vector2i currentSelectedTile{-1,-1};

		tgui::Button::Ptr btnPlusX;
		tgui::Button::Ptr btnMoinsX;
		tgui::Label::Ptr lblGrosseurX;

		tgui::Button::Ptr btnPlusY;
		tgui::Button::Ptr btnMoinsY;
		tgui::Label::Ptr lblGrosseurY;

		tgui::Button::Ptr btnPlusItemMin;
		tgui::Button::Ptr btnMoinsItemMin;
		tgui::Label::Ptr lbltempsItemMin;

		tgui::Button::Ptr btnPlusItemMax;
		tgui::Button::Ptr btnMoinsItemMax;
		tgui::Label::Ptr lbltempsItemMax;
		tgui::EditBox::Ptr editBox;
		tgui::Button::Ptr button;

		void exportToDatabase();
		bool keyBoardControls{ false };

};

#endif // !LEVEL_EDITOR