#ifndef __TILEMAP__
#define __TILEMAP__

#include <SFML/Graphics.hpp>
#include "../../DAO_Cpp/Block.h"
#include "../../DAO_Cpp/DTO_CPP.h"
#include "Arbre.h"
#include <vector>

class TileMap: public sf::Drawable, public sf::Transformable {
	public:
		TileMap(const std::string &imageLocations, sf::Vector2f tileSize = sf::Vector2f(256, 256), sf::Vector2u gridSize = sf::Vector2u(6, 6));
		~TileMap() = default;

		void setGridSize(sf::Vector2u gridSize);
		void setTileSize(sf::Vector2f tileSize);
		void setTileColor(sf::Color color, unsigned short i, unsigned short j);

		sf::Vector2u getGridSize();
		sf::Vector2f getTileSize();

		bool getPointCollision(sf::Vector2i &intersection, sf::Vector2i point);
		void update();
		
		Block& getTile(sf::Vector2u inter);
		sf::Vector2u getPlayerSpawnPoint(int playerID);
		void setPlayerSpawnPoint(int playerID, sf::Vector2u spawnPoint);
		int getNumberOfTrees();

		void setMapName(const std::string& name);
		void setMapName(const sf::String& name);
		void setMapStatus(Status state);
		Status getMapStatus();
		void setItemSpawnDelay(double min, double max);
		void setItemSpawnDelayMax(double max);
		void setItemSpawnDelayMin(double min);
		double getItemSpawnDelayMin();
		double getItemSpawnDelayMax();
		std::string getMapName();

		const sf::Vector2u getGridSizeMax();
		const sf::Vector2u getGridSizeMin();
		const int getMaxNbTrees();
	private:
		std::string mapName;
		bool load(const std::string &imageLocations, sf::Vector2f tileSize = sf::Vector2f(256, 256), sf::Vector2u gridSize = sf::Vector2u(6, 6));
		void build();
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		const sf::Vector2u GRID_SIZE_MAX = sf::Vector2u(12, 12);
		const sf::Vector2u GRID_SIZE_MIN = sf::Vector2u(6, 6);
		const sf::Vector2f TILE_SIZE_MAX = sf::Vector2f(256, 256);
		const int MAX_NUMBER_TREES{ 4 };
		const double ITEM_SPAWN_DELAY_MIN{ 3.0 };
		const double ITEM_SPAWN_DELAY_MAX{ 20.0 };

		sf::Vector2f tileSize;
		sf::Vector2u gridSize;
		sf::VertexArray m_vertices;

		sf::Texture m_texTree;
		sf::Texture m_texture;

		Arbres arbres;
		Arbres murs;
		Arbres mobiles;
		Arbres invs;

		std::vector<std::vector<Block>> _tileset;

		Status mapStatus;
		double itemSpawnDelayMax;
		double itemSpawnDelayMin;
		std::vector<sf::Vector2u> _playerSpawnPoints;
		void _resizeTileMap(int x, int y);
		
		const int NB_PLAYERS_MAX{ 2 };
		bool livin_tree[144] = { false };
		bool livin_murs[144] = { false };
		bool livin_mobiles[144] = { false };
		bool livin_invs[144] = { false };

		void initData();
		void initVector(sf::Vector2u gridSize);
};

#endif // !TILEMAP