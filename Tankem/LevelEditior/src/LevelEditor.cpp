#include "..\..\LevelEditior\include\LevelEditor.h"
#include "..\..\DAO_Cpp\DAO_CSV_CPP.h"
#include <math.h>
#include <time.h>
#include <ctime>

LevelEditor::LevelEditor():
	window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Level Editor pour programmeur avangardiste"),
	gui(window),
	tilemap("asset/image/Floor/tex/Floor.png", sf::Vector2f(64, 64), sf::Vector2u(6, 6)),
	alertManager("asset/font/Minecraft.ttf",32, window.getSize())
{
	window.setFramerateLimit(30);

	tilemap.setTileColor(sf::Color(200, 0, 200), 2, 3);
	tilemap.setTileColor(sf::Color(200, 255, 200), 1, 3);
	tilemap.setTileColor(sf::Color(255, 255, 255), 0, 3);

	tilemap.setPosition(sf::Vector2f(SCREEN_WIDTH/2 - (tilemap.getGridSize().x * tilemap.getTileSize().x)/2,
									SCREEN_HEIGHT/2 - (tilemap.getGridSize().y * tilemap.getTileSize().y)/2));

	tilemap.setGridSize(sf::Vector2u(12, 12));
	tilemap.setTileSize(sf::Vector2f(32, 32));

	button = tgui::Button::create();
	editBox = tgui::EditBox::create();

	button->setText("Send to Database");
	editBox->setText("hello world");
	gui.add(button);
	//gui.add(editBox);
	editBox->setPosition(window.getSize().x - 100, 0);
	gui.add(editBox);

	//button->connect("pressed", [&]() { window.close(); }); //Closes the window
	button->connect("pressed", [&]() { exportToDatabase(); });
	//button->connect("TextChanged", [&]() { tilemap.setMapName(editBox->getText());  });

	editBox->connect("TextChanged", [&]() {
		tilemap.setMapName(editBox->getText().getData());
	});

	btnPlusX = tgui::Button::create();
	btnMoinsX = tgui::Button::create();
	lblGrosseurX = tgui::Label::create();

	btnPlusY = tgui::Button::create();
	btnMoinsY = tgui::Button::create();
	lblGrosseurY = tgui::Label::create();

	lblGrosseurX->setText("   GrosseurX   ");
	btnPlusX->setText("+");
	btnMoinsX->setText("-");
	lblGrosseurX->getRenderer()->setBackgroundColor(sf::Color(128, 128, 168));


	lblGrosseurY->setText("   GrosseurY   ");
	btnPlusY->setText("+");
	btnMoinsY->setText("-");
	lblGrosseurY->getRenderer()->setBackgroundColor(sf::Color(128, 128, 168));

	btnPlusX->setPosition(sf::Vector2f(0, 100));
	lblGrosseurX->setPosition(btnPlusX->getPosition() + sf::Vector2f(50, 0));
	btnMoinsX->setPosition(lblGrosseurX->getPosition() + sf::Vector2f(100, 0));

	btnPlusY->setPosition(sf::Vector2f(0, 200));
	lblGrosseurY->setPosition(btnPlusY->getPosition() + sf::Vector2f(50, 0));
	btnMoinsY->setPosition(lblGrosseurY->getPosition() + sf::Vector2f(100, 0));

	btnPlusX->connect("pressed", [&]() {
		tilemap.setGridSize(sf::Vector2u(tilemap.getGridSize().x + 1, tilemap.getGridSize().y));


		//re-colorisation du tile current si possible
		if (currentSelectedTile.x >= tilemap.getGridSize().x || currentSelectedTile.y >= tilemap.getGridSize().y) {
			p_tileGui = nullptr;
			currentSelectedTile = sf::Vector2i(-1, -1);
		}
		else if (currentSelectedTile.x != -1)
			tilemap.setTileColor(sf::Color(64, 64, 255), currentSelectedTile.x, currentSelectedTile.y);
	});


	btnMoinsX->connect("pressed", [&]() {
		tilemap.setGridSize(sf::Vector2u(tilemap.getGridSize().x - 1, tilemap.getGridSize().y));


		//re-colorisation du tile current si possible
		if (currentSelectedTile.x >= tilemap.getGridSize().x || currentSelectedTile.y >= tilemap.getGridSize().y) {
			p_tileGui = nullptr;

			currentSelectedTile = sf::Vector2i(-1, -1);
		}
		else if (currentSelectedTile.x != -1)
			tilemap.setTileColor(sf::Color(64, 64, 255), currentSelectedTile.x, currentSelectedTile.y);
	});

	btnPlusY->connect("pressed", [&]() {
		tilemap.setGridSize(sf::Vector2u(tilemap.getGridSize().x, tilemap.getGridSize().y + 1));

		//re-colorisation du tile current si possible
		if (currentSelectedTile.x >= tilemap.getGridSize().x || currentSelectedTile.y >= tilemap.getGridSize().y) {
			p_tileGui = nullptr;

			currentSelectedTile = sf::Vector2i(-1, -1);
		}
		else if (currentSelectedTile.x != -1)
			tilemap.setTileColor(sf::Color(64, 64, 255), currentSelectedTile.x, currentSelectedTile.y);
	});

	btnMoinsY->connect("pressed", [&]() {
		tilemap.setGridSize(sf::Vector2u(tilemap.getGridSize().x, tilemap.getGridSize().y - 1));

		//re-colorisation du tile current si possible
		if (currentSelectedTile.x >= tilemap.getGridSize().x || currentSelectedTile.y >= tilemap.getGridSize().y) {
			p_tileGui = nullptr;

			currentSelectedTile = sf::Vector2i(-1, -1);
		}
		else if (currentSelectedTile.x != -1)
			tilemap.setTileColor(sf::Color(64, 64, 255), currentSelectedTile.x, currentSelectedTile.y);
	});

	gui.add(btnPlusX);
	gui.add(btnMoinsX);
	gui.add(lblGrosseurX);

	gui.add(btnPlusY);
	gui.add(btnMoinsY);
	gui.add(lblGrosseurY);

	btnPlusItemMin = tgui::Button::create();
	btnMoinsItemMin = tgui::Button::create();
	lbltempsItemMin = tgui::Label::create();

	lbltempsItemMin->setText("   tempMin   " + std::to_string(tilemap.getItemSpawnDelayMin()) + "s");
	btnPlusItemMin->setText("+");
	btnMoinsItemMin->setText("-");
	lbltempsItemMin->getRenderer()->setBackgroundColor(sf::Color(128, 128, 168));
				
	btnPlusItemMax = tgui::Button::create();
	btnMoinsItemMax = tgui::Button::create();
	lbltempsItemMax = tgui::Label::create();
				
	lbltempsItemMax->setText("   tempMax   " + std::to_string(tilemap.getItemSpawnDelayMax()) + "s");
	btnPlusItemMax->setText("+");
	btnMoinsItemMax->setText("-");
	lbltempsItemMax->getRenderer()->setBackgroundColor(sf::Color(128, 128, 168));

	btnPlusItemMin->setPosition(sf::Vector2f(0, 300));
	btnMoinsItemMin->setPosition(btnPlusItemMin->getPosition() + sf::Vector2f(50, 0));
	lbltempsItemMin->setPosition(btnMoinsItemMin->getPosition() + sf::Vector2f(100, 0));

	btnPlusItemMax->setPosition(sf::Vector2f(0, 400));
	btnMoinsItemMax->setPosition(btnPlusItemMax->getPosition() + sf::Vector2f(50, 0));
	lbltempsItemMax->setPosition(btnMoinsItemMax->getPosition() + sf::Vector2f(100, 0));

	btnPlusItemMin->connect("pressed", [&]() {
		tilemap.setItemSpawnDelayMin(tilemap.getItemSpawnDelayMin() + 1);
		lbltempsItemMin->setText("   tempMin   " + std::to_string(tilemap.getItemSpawnDelayMin()) + "s");
	});

	btnMoinsItemMin->connect("pressed", [&]() {
		tilemap.setItemSpawnDelayMin(tilemap.getItemSpawnDelayMin() - 1);
		lbltempsItemMin->setText("   tempMin   " + std::to_string(tilemap.getItemSpawnDelayMin()) + "s");
	});

	btnPlusItemMax->connect("pressed", [&]() {
		tilemap.setItemSpawnDelayMax(tilemap.getItemSpawnDelayMax() + 1);
		lbltempsItemMax->setText("   tempMax   " + std::to_string(tilemap.getItemSpawnDelayMax()) + "s");
	});

	btnMoinsItemMax->connect("pressed", [&]() {
		tilemap.setItemSpawnDelayMax(tilemap.getItemSpawnDelayMax() - 1);
		lbltempsItemMax->setText("   tempMax   " + std::to_string(tilemap.getItemSpawnDelayMax()) + "s");
	});

	gui.add(btnPlusItemMin);
	gui.add(btnMoinsItemMin);
	gui.add(lbltempsItemMin);

	gui.add(btnPlusItemMax);
	gui.add(btnMoinsItemMax);
	gui.add(lbltempsItemMax);
}

void LevelEditor::run() {
	while (window.isOpen()) {
		event();
		update();
		draw();
	}
};

void LevelEditor::event() {

	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window.close();
		if (event.type == sf::Event::MouseWheelMoved) {
			tilemap.setTileSize(tilemap.getTileSize() + sf::Vector2f(event.mouseWheel.delta, event.mouseWheel.delta));
		
			//re-colorisation du tile current si possible
			if (currentSelectedTile.x >= tilemap.getGridSize().x || currentSelectedTile.y >= tilemap.getGridSize().y) {
				p_tileGui = nullptr;
				currentSelectedTile = sf::Vector2i(-1, -1);
			}
			else if (currentSelectedTile.x != -1)
				tilemap.setTileColor(sf::Color(64, 64, 255), currentSelectedTile.x, currentSelectedTile.y);
		}
	}
	//evennement du GUI
	gui.handleEvent(event);

	//evennement souris sur TileMap
	sf::Vector2i mouse_pos = sf::Mouse::getPosition(window);
	sf::Vector2i grid_intersection;
	static sf::Vector2i prev_grid_intersection = sf::Vector2i(0,0);
	static sf::Vector2i prev_mouse_pos = mouse_pos;

	//s'assurer que le tile redevient a sa couleur normale
	if(currentSelectedTile.x != prev_grid_intersection.x || currentSelectedTile.y != prev_grid_intersection.y)
		tilemap.setTileColor(sf::Color(255, 255, 255), prev_grid_intersection.x, prev_grid_intersection.y);

	//modifier la couleur du tile courant s'il y a lieu
	if (tilemap.getPointCollision(grid_intersection, mouse_pos)) {
		bool isTileGuiInit = this->p_tileGui ? true : false;
		bool isMouseOnTileGui = false;

		if (isTileGuiInit)
			if (this->p_tileGui->getGlobalBound().contains(sf::Vector2f(mouse_pos.x, mouse_pos.y)))
				isMouseOnTileGui = true;
			else
				isMouseOnTileGui = false;

		//KeyboardControls
		/*
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
			keyBoardControls = true;
			if (currentSelectedTile.y > 0) {
				currentSelectedTile.y--;
			}
			else {
				grid_intersection.y = 0;
				if (currentSelectedTile.x == -1) {
					currentSelectedTile.x = 0;
				}
			}

		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
			keyBoardControls = true;
			if (currentSelectedTile.y < tilemap.getGridSize().y) {
				currentSelectedTile.y++;
			}
			else {
				currentSelectedTile.y = 0;
				if (currentSelectedTile.x == -1) {
					currentSelectedTile.x = 0;
				}
			}
		}
		*/

		//onClick
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			if (!isMouseOnTileGui) {
				std::string name = "Block " + std::to_string(grid_intersection.x) + ", " + std::to_string(grid_intersection.y);
				if (currentSelectedTile != grid_intersection) {
					tilemap.setTileColor(sf::Color(255, 255, 255), currentSelectedTile.x, currentSelectedTile.y);
					currentSelectedTile = grid_intersection;
				}

				this->p_tileGui = std::make_unique<TileGui>(&tilemap.getTile(sf::Vector2u(currentSelectedTile)), window, sf::Vector2i(mouse_pos.x + 50, mouse_pos.y), name);

				tilemap.setTileColor(sf::Color(64, 64, 255), grid_intersection.x, grid_intersection.y);
			}		
			//on click + move
			sf::Vector2i mouse_dist = mouse_pos - prev_mouse_pos;
			if (mouse_dist != sf::Vector2i(0, 0)) {
				tilemap.setPosition(tilemap.getPosition() + sf::Vector2f(mouse_dist.x, mouse_dist.y));
				if(tilemap.getPosition().x <  -(tilemap.getGridSize().y * tilemap.getTileSize().y - 50) || tilemap.getPosition().x > SCREEN_WIDTH - 50)
					tilemap.setPosition(tilemap.getPosition() + sf::Vector2f(-mouse_dist.x, 0));
				if (tilemap.getPosition().y < -(tilemap.getGridSize().y * tilemap.getTileSize().y - 50) || tilemap.getPosition().y > SCREEN_HEIGHT - 50)
					tilemap.setPosition(tilemap.getPosition() + sf::Vector2f(0, -mouse_dist.y));
			}
		}

		//hover
		else if(!isMouseOnTileGui){
			if(currentSelectedTile != grid_intersection)
				tilemap.setTileColor(sf::Color(128, 128, 255), grid_intersection.x, grid_intersection.y);
		}
	}

	//sauvegarde tile present s'il n'est pas invalide
	if(grid_intersection.x != -1)
		prev_grid_intersection = grid_intersection;

	prev_mouse_pos = mouse_pos;


	//change la taille de la grille
	static sf::Vector2i origin(0, 0);
	static bool hasScaleStart = false;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		if (!hasScaleStart) {
			origin = mouse_pos;
			hasScaleStart = true;
		}

		sf::Vector2i distv = mouse_pos - sf::Vector2i(tilemap.getPosition().x, tilemap.getPosition().y);

		if (distv.x < 0)
			distv.x *= -1;
		if (distv.y < 0)
			distv.y *= -1;

		distv.x /= tilemap.getTileSize().x;
		distv.y /= tilemap.getTileSize().y;

		tilemap.setGridSize(sf::Vector2u(distv.x, distv.y));

		//re-colorisation du tile current si possible
		if (currentSelectedTile.x >= tilemap.getGridSize().x || currentSelectedTile.y >= tilemap.getGridSize().y) {
			p_tileGui = nullptr;

			currentSelectedTile = sf::Vector2i(-1, -1);
		}else if(currentSelectedTile.x != -1)
			tilemap.setTileColor(sf::Color(64, 64, 255), currentSelectedTile.x, currentSelectedTile.y);
	}

	//handle les events selon le gui de tile s'il existe
	if (p_tileGui) {
		p_tileGui->event(event);
	}

};

void LevelEditor::update() {
	tilemap.update();
	if (p_tileGui) {
		p_tileGui->update();
		if (p_tileGui->hasPurpose())
			p_tileGui = nullptr;
	}
	if (tilemap.getNumberOfTrees() > 4)
		p_tileGui->uncheckArbre();
	alertManager.update();
};

void LevelEditor::draw() {
	window.clear();
	window.draw(tilemap);
	gui.draw();
	if(p_tileGui)
		p_tileGui->draw();
	window.draw(alertManager);
	window.display();
};

void LevelEditor::exportToDatabase(){
	bool success = true;
	sf::Vector2u gridSize = tilemap.getGridSize();
	sf::Vector2u gridSizeMax = tilemap.getGridSizeMax();
	sf::Vector2u gridSizeMin = tilemap.getGridSizeMin();
	
	//Verify grid size limitations
	if (gridSize.x < gridSizeMin.x || gridSize.y < gridSizeMin.y) {
		alertManager.addAlert("La taille du canevas doit etre de 6x6 minimum.");
		success = false;
	}
	if (gridSize.x > gridSizeMax.x || gridSize.y > gridSizeMax.y) {
		alertManager.addAlert("La taille du canevas doit etre de 12x12 maximum.");
		success = false;
	}

	//Arbres
	if (tilemap.getNumberOfTrees() > tilemap.getMaxNbTrees()) {
		std::string s = "Vous n'avez le droit qu'a " + tilemap.getMaxNbTrees() ;
		s += "arbres sur la map au maximum.";
		alertManager.addAlert(s);
		success = false;
	}

	//Check SpawnPoints
	if (tilemap.getPlayerSpawnPoint(1).x > gridSize.x || tilemap.getPlayerSpawnPoint(1).y > gridSize.y) {
		alertManager.addAlert("Le joueur 1 ne spawn pas sur une case disponible.");
		success = false;
	}
	if (tilemap.getPlayerSpawnPoint(2).x > gridSize.x || tilemap.getPlayerSpawnPoint(2).y > gridSize.y) {
		alertManager.addAlert("Le joueur 2 ne spawn pas sur une case disponible.");
		success = false;
	}


	if (success) {
		DTO dto(tilemap.getMapName(), gridSize.x, gridSize.y, tilemap.getMapStatus());
		std::string result;
		/*Formating the time into a string*/
		/*https://stackoverflow.com/questions/16357999/current-date-and-time-as-string*/
		time_t rawtime;
		struct tm timeinfo;
		char buffer[80];
		time(&rawtime);
		localtime_s(&timeinfo, &rawtime);
		strftime(buffer, sizeof(buffer), "%Y-%m-%d", &timeinfo);

		dto.date = buffer;
		dto.delais_apparition_max = tilemap.getItemSpawnDelayMax();
		dto.delais_apparition_min = tilemap.getItemSpawnDelayMin();
		dto.pos_joueur1_x = tilemap.getPlayerSpawnPoint(1).x;
		dto.pos_joueur1_y = tilemap.getPlayerSpawnPoint(1).y;
		dto.pos_joueur2_x = tilemap.getPlayerSpawnPoint(2).x;
		dto.pos_joueur2_y = tilemap.getPlayerSpawnPoint(2).y;

		tilemap.getTile(sf::Vector2u{ 5, 5 }).setBlock(Block_type::MUR_FIXE, true);

		for (int i = 0; i < gridSize.x; i++) {
			for (int j = 0; j < gridSize.y; j++) {
				sf::Vector2u vec(i, j);
				Block& tile = tilemap.getTile(vec);
				dto.terrain[j][i] = tile;
			}
		}
		result = DAO_CSV::write(dto);

		alertManager.addAlert(result);
	}
	else {
		alertManager.addAlert("Unable");
	}
}
