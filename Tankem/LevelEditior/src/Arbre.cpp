#include "..\include\Arbre.h"
#include <iostream>


Arbres::Arbres(const std::string & fileLocation, float tileSize, sf::Vector2u gridSize)
{
	if (m_texture.loadFromFile(fileLocation)) {
		for (int i = 0; i < 12; i++)
			for (int j = 0; j < 12; j++) {
				m_sprite_arbres[i][j] = Arbre(&m_texture, sf::Vector2i(i, j), tileSize);
			}
	}

	this->tileSize = tileSize;
	this->gridSize = gridSize;
}

void Arbres::addArbre(unsigned int i, unsigned int j)
{
	Arbre temp(&m_texture, sf::Vector2i(i, j), this->tileSize);

	m_sprite_arbres[i][j].onScreen = true;
}

void Arbres::removeArbre(int i, int j)
{
	this->m_sprite_arbres[i][j].onScreen = false;
}

void Arbres::setTileSize(float tileSize)
{
	this->tileSize = tileSize;

	for(int i = 0; i < 12; i++)
		for(int j = 0; j < 12; j++){
			m_sprite_arbres[i][j].size = this->tileSize;
			m_sprite_arbres[i][j].build();
		}
	
}

void Arbres::setGridSize(sf::Vector2u gridSize)
{
	this->gridSize = gridSize;
	for (int i = 0; i < 12; i++)
		for (int j = 0; j < 12; j++)
			if (m_sprite_arbres[i][j].onScreen)
				if (i >= gridSize.x || j >= gridSize.y)
					m_sprite_arbres[i][j].onScreen = false;
				
}

void Arbres::setArbreSize(float tileSize)
{
	this->tileSize = tileSize;
}

void Arbres::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	states.transform *= getTransform();	
	for (int i = 0; i < 12; i++)
		for (int j = 0; j < 12; j++) {
			if (m_sprite_arbres[i][j].onScreen)
				if (i < gridSize.x && j < gridSize.y)
					target.draw(m_sprite_arbres[i][j], states);
			
		}
}

Arbre::Arbre()
{
	this->size = 1;
	this->ID = sf::Vector2i(-1, -1);

}

Arbre::Arbre(sf::Texture* texture, sf::Vector2i ID, float tileSize)
{
	this->size = tileSize;
	std::cout << "loading sprite: " << ID.x << ", " << ID.y << std::endl;
	m_texture = texture;
	this->onScreen = false;
	this->ID = ID;

	build();
}



void Arbre::build()
{
	m_vertices.setPrimitiveType(sf::Quads);
	m_vertices.resize(4);

	float texture_width = m_texture->getSize().x;
	float texture_height = m_texture->getSize().y;

	m_vertices[0].position = sf::Vector2f(ID.x * size,		 ID.y * size);
	m_vertices[1].position = sf::Vector2f((ID.x + 1) * size, ID.y * size);
	m_vertices[2].position = sf::Vector2f((ID.x + 1) * size, (ID.y + 1) * size);
	m_vertices[3].position = sf::Vector2f(ID.x * size,		 (ID.y + 1) * size);


	m_vertices[0].texCoords = sf::Vector2f(0, 0);
	m_vertices[1].texCoords = sf::Vector2f(texture_width, 0);
	m_vertices[2].texCoords = sf::Vector2f(texture_width, texture_height);
	m_vertices[3].texCoords = sf::Vector2f(0, texture_height);
}

void Arbre::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = m_texture;
	target.draw(m_vertices, states);
}

