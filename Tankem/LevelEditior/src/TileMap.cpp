#include "..\..\LevelEditior\include\TileMap.h"
#include <iostream>
#include <random>

TileMap::TileMap(const std::string &imageLocations, sf::Vector2f tileSize, sf::Vector2u gridSize):
	arbres("asset/image/Decoration/arbre.png", tileSize.x, gridSize),
	murs("asset/image/Decoration/mur.png", tileSize.x, gridSize),
	mobiles("asset/image/Decoration/murbouge.png", tileSize.x, gridSize),
	invs("asset/image/Decoration/murbougeInv.png", tileSize.x, gridSize)
{
	load(imageLocations, tileSize, gridSize);
}

const sf::Vector2u TileMap::getGridSizeMax()
{
	return GRID_SIZE_MAX;
}

const sf::Vector2u TileMap::getGridSizeMin()
{
	return GRID_SIZE_MIN;
}

const int TileMap::getMaxNbTrees()
{
	return MAX_NUMBER_TREES;
}

bool TileMap::load(const std::string &imageLocations, sf::Vector2f tileSize, sf::Vector2u gridSize) {
	m_vertices.setPrimitiveType(sf::Quads);

	initData();
	setGridSize(gridSize);
	setTileSize(tileSize);
	if (!m_texture.loadFromFile(imageLocations))
		return false;

	build();
	return true;
}

void TileMap::build()
{
	m_vertices.resize(this->gridSize.x * this->gridSize.y * 4);

	float texture_width = m_texture.getSize().x;
	float texture_height = m_texture.getSize().y;

	for (unsigned int i = 0; i < this->gridSize.x; i++)
		for (unsigned int j = 0; j < this->gridSize.y; j++)
		{
			unsigned short currTile = i + j * this->gridSize.x;

			sf::Vertex* quad = &m_vertices[currTile * 4];

			quad[0].position = sf::Vector2f(i * this->tileSize.x, j * this->tileSize.y);
			quad[1].position = sf::Vector2f((i + 1) * this->tileSize.x, j * this->tileSize.y);
			quad[2].position = sf::Vector2f((i + 1) * this->tileSize.x, (j + 1) * this->tileSize.y);
			quad[3].position = sf::Vector2f(i * this->tileSize.x, (j + 1) * this->tileSize.y);

			quad[0].texCoords = sf::Vector2f(0, 0);
			quad[1].texCoords = sf::Vector2f(texture_width, 0);
			quad[2].texCoords = sf::Vector2f(texture_width, texture_height);
			quad[3].texCoords = sf::Vector2f(0, texture_height);

			quad[0].color = sf::Color::White;
			quad[1].color = sf::Color::White;
			quad[2].color = sf::Color::White;
			quad[3].color = sf::Color::White;
		}

}

void TileMap::update() {
	
	for (int i = 0; i < _tileset.size(); i++)
		for (int j = 0; j < _tileset[i].size(); j++)
		{
			
			if (_tileset[i][j].getArbre() && !livin_tree[i + j * gridSize.x]) {
					arbres.addArbre(j, i);
					livin_tree[i + j * gridSize.x] = true;

			}
			else if (!_tileset[i][j].getArbre() && livin_tree[i + j * gridSize.x]) {
				arbres.removeArbre(j, i);
				livin_tree[i + j * gridSize.x] = false;
			}

			bool isMur = _tileset[i][j].getType() == Block_type::MUR_FIXE || _tileset[i][j].getType() == Block_type::MUR_VERTICAL || _tileset[i][j].getType() == Block_type::MUR_VERTICAL_INVERSE;
			bool isMobile = _tileset[i][j].getType() == Block_type::MUR_VERTICAL;
			bool isMobileInv = _tileset[i][j].getType() == Block_type::MUR_VERTICAL_INVERSE;
			

			if (isMur && !livin_murs[i + j * gridSize.x]) {
				murs.addArbre(j, i);
				livin_murs[i + j * gridSize.x] = true;
			}

			else if (!isMur && livin_murs[i + j * gridSize.x]) {
				murs.removeArbre(j, i);
				livin_murs[i + j * gridSize.x] = false;
			}

			if (isMobile && !livin_mobiles[i + j * gridSize.x]) {
				mobiles.addArbre(j, i);
				livin_mobiles[i + j * gridSize.x] = true;
			}

			else if (!isMobile && livin_mobiles[i + j * gridSize.x]) {
				mobiles.removeArbre(j, i);
				livin_mobiles[i + j * gridSize.x] = false;
			}

			if (isMobileInv && !livin_invs[i + j * gridSize.x]) {
				invs.addArbre(j, i);
				livin_invs[i + j * gridSize.x] = true;
			}

			else if (!isMobileInv && livin_invs[i + j * gridSize.x]) {
				invs.removeArbre(j, i);
				livin_invs[i + j * gridSize.x] = false;
			}
		}

}

void TileMap::setGridSize(sf::Vector2u gridSize) {

	//clamp la nouvelle taille propose avant l'assignation
	if (gridSize.x > GRID_SIZE_MAX.x)
		gridSize.x = GRID_SIZE_MAX.x;
	if (gridSize.y > GRID_SIZE_MAX.y)
		gridSize.y = GRID_SIZE_MAX.y;
	if (gridSize.x < GRID_SIZE_MIN.x)
		gridSize.x = GRID_SIZE_MIN.x;
	if (gridSize.y < GRID_SIZE_MIN.y)
		gridSize.y = GRID_SIZE_MIN.y;
	/*
	if (gridSize.x < 1)
		gridSize.x = 1;
	if (gridSize.y < 1)
		gridSize.y = 1;
	*/

	arbres.setGridSize(gridSize);
	murs.setGridSize(gridSize);
	invs.setGridSize(gridSize);
	mobiles.setGridSize(gridSize);

	this->gridSize = gridSize;

	//modifie reellement l'image
	build();

	_resizeTileMap(gridSize.x, gridSize.y);
}

void TileMap::setTileSize(sf::Vector2f tileSize) {

	//clamp la nouvelle taille propose avant l'assignation
	if (tileSize.x > TILE_SIZE_MAX.x)
		tileSize.x = TILE_SIZE_MAX.x;
	if (tileSize.y > TILE_SIZE_MAX.y)
		tileSize.y = TILE_SIZE_MAX.y;
	if (tileSize.x < 1)
		tileSize.x = 1;
	if (tileSize.y < 1)
		tileSize.y = 1;

	this->tileSize = tileSize;
	arbres.setTileSize(tileSize.x);
	murs.setTileSize(tileSize.x);
	mobiles.setTileSize(tileSize.x);
	invs.setTileSize(tileSize.x);
	//modifie reellement l'image
	build();
}

void TileMap::setTileColor(sf::Color color, unsigned short i, unsigned short j) {
	if (i >= gridSize.x || j >= gridSize.y)
		return;

	unsigned short currTile = i + j * this->gridSize.x;

	sf::Vertex* quad = &m_vertices[currTile * 4];

	quad[0].color = color;
	quad[1].color = color;
	quad[2].color = color;
	quad[3].color = color;
}

sf::Vector2u TileMap::getGridSize(){
	return gridSize;
}

sf::Vector2f TileMap::getTileSize(){
	return tileSize;
}

bool TileMap::getPointCollision(sf::Vector2i &intersection, sf::Vector2i point) {
	bool success = true;

	//position du rectangle en position global dans la fenetre
	int x = this->getPosition().x;
	int y = this->getPosition().y;
	int w = this->gridSize.x * this->tileSize.x;
	int h = this->gridSize.y * this->tileSize.y;

	//verification et modification de l'insertion 
	if (point.x > x && point.x < x + w && point.y > y && point.y < y + h) {
		intersection.x = point.x - x;
		intersection.x /= this->tileSize.x;

		intersection.y = point.y - y;
		intersection.y /= this->tileSize.y;
	}
	else {
		intersection.x = -1;
		intersection.y = -1;
		success = false;
	}
	return success;
}

void TileMap::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = &m_texture;
	target.draw(m_vertices, states);
	target.draw(arbres, states);
	target.draw(murs, states);
	target.draw(invs, states);
	target.draw(mobiles, states);
}

void TileMap::_resizeTileMap(int x, int y) {
	//X-> (Horizontal)
	//Y^  (Vertical)
	int currentMapSizeY = _tileset.size();
	int currentMapSizeX;
	if(currentMapSizeY > 0) {
		currentMapSizeX = _tileset.at(0).size();
	}
	else {
		currentMapSizeX = 0;
	}

	/*Y Coordinates*/
	if (y > currentMapSizeY) {
		for (int i = currentMapSizeY; i < y; i++) {
			_tileset.push_back(std::vector<Block>());
			for (int k = 0; k < x; k++) {
				_tileset[i].push_back(Block());
			}
		}
	}
	else if (y < currentMapSizeY) {
		try {
			_tileset.erase(_tileset.begin() + y, _tileset.end());
		}
		catch (const std::out_of_range& e) {
		}
	}

	/*X Coordinates*/
	if (x > currentMapSizeX) {
		
		for (int i = currentMapSizeX; i < x; i++) {
			for (int j = 0; j < y; j++) {
				if (_tileset.at(j).size() < x){
					_tileset.at(j).push_back(Block());
				}
			}
		}
	}
	else {
		for (int j = 0; j < y; j++) {
			std::vector<Block>&tiles = _tileset.at(j);
			if (tiles.size() >= x) {
				tiles.erase(tiles.begin() + x, tiles.end());
			}
			else {
				for (int k = tiles.size(); k < x; k++) {
					tiles.push_back(Block());
				}
			}
		}
	}

	/*Check for player spawn points out of bounds*/
	for (int i = 0; i > _playerSpawnPoints.size(); i++) {
		sf::Vector2u &vec{ _playerSpawnPoints.at(i) };
		setPlayerSpawnPoint(i + 1, vec);
	}
}

int TileMap::getNumberOfTrees()
{
	int nbTrees = 0;
	for (int i = _tileset.size() - 1; i >= 0; i--) {
		for (int j = _tileset.at(i).size() - 1; j >= 0; j--) {
			try {
				Block& tile = _tileset.at(j).at(i);
				if (tile.getArbre()) {
					nbTrees++;
				}
			}
			catch (const std::out_of_range& e) {
			}
		}
	}
	return nbTrees;
}

void TileMap::setMapName(const std::string& name){
	mapName = name;
}

void TileMap::setMapName(const sf::String& name){
	mapName = name;
}
void TileMap::setMapStatus(Status state)
{
	mapStatus = state;
}
Status TileMap::getMapStatus()
{
	return mapStatus;
}
void TileMap::setItemSpawnDelay(double min, double max)
{
	setItemSpawnDelayMin(min);
	setItemSpawnDelayMax(max);
}
void TileMap::setItemSpawnDelayMax(double max)
{
	itemSpawnDelayMax = max;
}
void TileMap::setItemSpawnDelayMin(double min)
{
	itemSpawnDelayMin = min;
}
double TileMap::getItemSpawnDelayMin()
{
	return itemSpawnDelayMin;
}
double TileMap::getItemSpawnDelayMax()
{
	return itemSpawnDelayMax;
}
std::string TileMap::getMapName(){
	return mapName;
}

void TileMap::initData()
{
	initVector(GRID_SIZE_MAX);
	for (int i = 0; i > NB_PLAYERS_MAX; i++) {
		_playerSpawnPoints.push_back(sf::Vector2u{ rand() % GRID_SIZE_MAX.x, rand() % GRID_SIZE_MAX.y });
	}
	mapName = "Map Name";
	mapStatus = Status::TEST;
	itemSpawnDelayMin = 3.0;
	itemSpawnDelayMax = 20.0;
}

void TileMap::initVector(sf::Vector2u gridSize)
{
	for (int j = 0; j < gridSize.y; j++) {
		_tileset.push_back(std::vector<Block>());
		for (int i = 0; i < gridSize.x; i++) {
			_tileset.at(j).push_back(Block());
		}
	}
}


Block& TileMap::getTile(sf::Vector2u inter) {
	if (inter.x <= gridSize.x && inter.y <= gridSize.y) {
		try {
			Block& tile = _tileset.at(inter.y).at(inter.x);
			return tile;
		}
		catch (const std::out_of_range& e){
			_resizeTileMap(gridSize.x, gridSize.y);
			Block& tile = _tileset.at(inter.y).at(inter.x);
			return tile;
		}
	}
}

sf::Vector2u TileMap::getPlayerSpawnPoint(int playerID){
	try {
		sf::Vector2u &vec = _playerSpawnPoints.at(playerID - 1);
		return vec;
	}
	catch (const std::out_of_range& e) {
		return sf::Vector2u{ 0, 0 }; //IDK
	}
}

void TileMap::setPlayerSpawnPoint(int playerID, sf::Vector2u spawnPoint){
	try {
		sf::Vector2u &vec = _playerSpawnPoints.at(playerID-1);
		if (vec.x >= gridSize.x) {
			vec.x = gridSize.x - 1;
		}
		else if(vec.x < 0){
			vec.x = 0;
		}

		if (vec.y >= gridSize.y) {
			vec.y = gridSize.y - 1;
		}
		else if (vec.y < 0) {
			vec.y = 0;
		}
	}
	catch (const std::out_of_range& e) {
		_playerSpawnPoints.push_back(spawnPoint);
	}
}
