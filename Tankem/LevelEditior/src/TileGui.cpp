#include "..\include\TileGui.h"

TileGui::TileGui(Block* p_block, sf::RenderWindow& window, sf::Vector2i position, const std::string& name):
	gui(window)
{
	this->p_block = p_block;
	this->window = &window;
	this->name = name;

	this->position = sf::Vector2f(position.x, position.y);
	this->margin = sf::Vector2f(10,10);
	build();
}

void TileGui::build()
{
	buttonQuit = tgui::Button::create();
	buttonQuit->hide();

	labelName		= tgui::Label::create();
	labelMur		= tgui::Label::create();
	labelMurInverse = tgui::Label::create();
	labelMurMobile	= tgui::Label::create();
	labelArbre		= tgui::Label::create();

	checkBoxMur = tgui::CheckBox::create();
	checkBoxMurInverse = tgui::CheckBox::create();
	checkBoxMurMobile = tgui::CheckBox::create();
	checkBoxArbre = tgui::CheckBox::create();

	float secondColumn = 75;

	//design du nom
	labelName->getRenderer()->setTextColor(sf::Color::White);
	labelName->setPosition(this->margin + this->position);
	labelName->setText(this->name);

	//design du Quit button
	buttonQuit->setPosition(this->margin + this->position + sf::Vector2f(secondColumn + this->margin.x + 75, 0));
	buttonQuit->setText("X");
	buttonQuit->getRenderer()->setBackgroundColor(sf::Color::White);
	buttonQuit->getRenderer()->setTextColor(sf::Color::Blue);
	buttonQuit->getRenderer()->setBorderColor(sf::Color(64, 64, 255, 168));
	buttonQuit->getRenderer()->setBackgroundColorDown(sf::Color(64, 64, 255, 168));

	//design du mur
	labelMur->getRenderer()->setTextColor(sf::Color::White);
	labelMur->setPosition(labelName->getPosition() + sf::Vector2f(0, labelName->getSize().y + this->margin.y));
	labelMur->setText("mur");
	checkBoxMur->setPosition(labelMur->getPosition() + sf::Vector2f(secondColumn + this->margin.x, 0));

	//design mur inverse
	labelMurInverse->getRenderer()->setTextColor(sf::Color::White);
	labelMurInverse->setPosition(labelMur->getPosition() + sf::Vector2f(0, labelMur->getSize().y + this->margin.y) );
	labelMurInverse->setText("Invers�");
	checkBoxMurInverse->setPosition(labelMurInverse->getPosition() + sf::Vector2f(secondColumn + this->margin.x, 0));

	//design mur mobile
	labelMurMobile->getRenderer()->setTextColor(sf::Color::White);
	labelMurMobile->setPosition(labelMurInverse->getPosition() + sf::Vector2f(0, labelMurInverse->getSize().y + this->margin.y));
	labelMurMobile->setText("Mobile");
	checkBoxMurMobile->setPosition(labelMurMobile->getPosition() + sf::Vector2f(secondColumn + this->margin.x, 0));

	//design arbre
	labelArbre->getRenderer()->setTextColor(sf::Color::White);
	labelArbre->setPosition(labelMurMobile->getPosition() + sf::Vector2f(0, labelMurMobile->getSize().y + this->margin.y));
	labelArbre->setText("Arbre");
	checkBoxArbre->setPosition(labelArbre->getPosition() + sf::Vector2f(secondColumn + this->margin.x, 0));

	//background
	background.setFillColor(sf::Color(64, 64, 168, 128));
	background.setPosition(this->position);

	sf::Vector2f marginY(0.0f, this->margin.y);

	background.setSize(sf::Vector2f(161.8033f, 100.f) + sf::Vector2f(this->margin.x * 5, this->margin.y * 5));

	background.setOutlineColor(sf::Color(32, 32, 128, 128));
	background.setOutlineThickness(1.5f);

	gui.add(buttonQuit);

	gui.add(labelName);
	gui.add(labelMur);
	gui.add(labelMurInverse);
	gui.add(labelMurMobile);
	gui.add(labelArbre);

	gui.add(checkBoxMur);
	gui.add(checkBoxMurInverse);
	gui.add(checkBoxMurMobile);
	gui.add(checkBoxArbre);

	//coche s'qui faut
	if (this->p_block->getArbre())
		checkBoxArbre->check();
	if (this->p_block->getType() == Block_type::MUR_FIXE) {
		checkBoxMur->check();
	}
	if (this->p_block->getType() == Block_type::MUR_VERTICAL) {
		checkBoxMurMobile->check();
	}
	if (this->p_block->getType() == Block_type::MUR_VERTICAL_INVERSE) {
		checkBoxMurInverse->check();
	}

}


void TileGui::update()
{
	if (!checkBoxMur->isChecked()) {
		checkBoxMurInverse->uncheck();
		checkBoxMurInverse->hide();
		labelMurInverse->hide();

		checkBoxMurMobile->uncheck();
		checkBoxMurMobile->hide();
		labelMurMobile->hide();
	}
	else {
		checkBoxMurInverse->show();
		labelMurInverse->show();

		checkBoxMurMobile->show();
		labelMurMobile->show();
	}

	if (checkBoxArbre->isChecked()) {
		this->p_block->setBlock(p_block->getType(), true);
	}
	else {
		this->p_block->setBlock(p_block->getType(), false);
	}
	if (checkBoxMur->isChecked()) {
		if (checkBoxMur->isChecked() && checkBoxMurInverse->isChecked() && checkBoxMurMobile->isChecked()) {
			this->p_block->setBlock(Block_type::MUR_VERTICAL_INVERSE, p_block->getArbre());
		}
		else if (checkBoxMur->isChecked() && checkBoxMurMobile->isChecked()) {
			this->p_block->setBlock(Block_type::MUR_VERTICAL, p_block->getArbre());
		}
		else
			this->p_block->setBlock(Block_type::MUR_FIXE, p_block->getArbre());
	}
	else {
		this->p_block->setBlock(Block_type::PLANCHER, p_block->getArbre());
	}

}

void TileGui::event(sf::Event event)
{
	gui.handleEvent(event);
}

void TileGui::draw()
{
	window->draw(background);
	gui.draw();
	//gui.get("")->cl
}

void TileGui::setMarginInElem(sf::Vector2f margin)
{
	this->margin = margin;
}

void TileGui::uncheckArbre()
{
	this->checkBoxArbre->uncheck();
	if(p_block)
		this->p_block->setBlock(p_block->getType(), false);
}

bool TileGui::hasPurpose()
{
	if (!p_block)
		return true;
	else
		return false;
}

sf::FloatRect TileGui::getGlobalBound()
{
	return background.getGlobalBounds();
}

TileGui::~TileGui()
{

}


