import cx_Oracle
from DTO import *
import datetime

class DAO(object):
	def lectureDonnee():
		oracle_connection = cx_Oracle.connect("e1125028","AAAaaa111","delta/decinfo.edu")
		oracle_cursor = oracle_connection.cursor()
		donnee1 = "SELECT pos_joueur1_x, pos_joueur1_y, pos_joueur2_x, pos_joueur2_y, id, largeur, hauteur, titre, delais_apparition_min, delais_apparition_max, status_id, date_creation FROM info_map"
		donnee2 = "SELECT map_id, block_id, pos_x, pos_y, arbre FROM liaison_block_map WHERE map_id = "
		donnee3 = "SELECT nom FROM status_list WHERE id = "
		donnee4 = "SELECT nom FROM block_type WHERE id ="
		oracle_cursor.execute(donnee1)
		table1 = oracle_cursor.fetchall()
		for i in table1:
			maps = MAP()
			posJoueur1X = i[0]
			posJoueur1Y = i[1]
			posJoueur2X = i[2]
			posJoueur2Y = i[3]
			maps.posX1 = posJoueur1X
			maps.posY1 = posJoueur1Y
			maps.posX2 = posJoueur2X
			maps.posY2 = posJoueur2Y
			maps.largeurMap = i[5]
			maps.hauteurMap = i[6]
			maps.nomNiveau = i[7]
			maps.delaisMinApp = i[8]
			maps.delaisMaxApp = i[9]
			maps.date = i[11].date()
			maps.idMaps = i[4]

			idMaps = i[4]
			donnee2Bis = donnee2+str(idMaps)
			oracle_cursor.execute(donnee2Bis)
			table2 = oracle_cursor.fetchall()

			idStatus = i[10]
			donnee3Bis = donnee3+str(idStatus)
			print(donnee2Bis)
			oracle_cursor.execute(donnee3Bis)
			table3 = oracle_cursor.fetchall()

			for k in table3:
				maps.status = k[0]

			for l in table2:
				case = Case()
				typeCase = l[1]
				donnee4Bis = donnee4+str(typeCase)
				oracle_cursor.execute(donnee4Bis)
				table4 = oracle_cursor.fetchall()
				for z in table4:
					case.typeCase = z[0]
				case.posXCase = l[2]
				case.posYCase = l[3]
				case.arbre = l[4]
				maps.case.append(case)
			
			DTO.maps.append(maps)
		#for r in DTO.maps:
			#print(r.case)
		


	def insertionMaps():

		maps = DTO.maps[0]
		oracle_connection = cx_Oracle.connect("e1125028","AAAaaa111","delta/decinfo.edu")
		oracle_cursor = oracle_connection.cursor()
		## Check if Map exists already
		query = "Select * from info_map where titre = '" + maps.nomNiveau + "' "
		oracle_cursor.execute(query)
		foundMap = oracle_cursor.fetchall()
		if foundMap:
			return "This map name already exists."
		else:
			nouvelleMaps = """INSERT INTO info_map(titre,date_creation,status_id,delais_apparition_min,delais_apparition_max,largeur,hauteur,pos_joueur1_x,pos_joueur1_y,pos_joueur2_x,pos_joueur2_y)
						VALUES('"""+maps.nomNiveau+"',TO_DATE('"+str(maps.date)+"','YYYY-MM-DD'),(SELECT id FROM status_list WHERE nom = '"+maps.status+"'),"+str(maps.delaisMinApp)+","+str(maps.delaisMaxApp)+","+str(maps.largeurMap)+","+str(maps.hauteurMap)+","+str(maps.posX1)+","+str(maps.posY1)+","+str(maps.posX2)+","+str(maps.posY2)+")"
			print(nouvelleMaps)
			oracle_cursor.execute(nouvelleMaps)
			print("does this work")
			
			for case in maps.case:
				if case.typeCase != "Plancher" or case.arbre == "TRUE":
					block = """INSERT INTO liaison_block_map(map_id,block_id,pos_x,pos_y,arbre) 
					VALUES((SELECT id FROM info_map WHERE titre = '"""+maps.nomNiveau+"'),(SELECT ID FROM block_type WHERE nom = '"+case.typeCase+"'),"+str(case.posXCase)+","+str(case.posYCase)+",'"+case.arbre+"')"

					oracle_cursor.execute(block)

		oracle_connection.commit()
		oracle_connection.close()
		return "Insertion de map effectuee correctement"


	def testConnection():
		try:
			oracle_connection = cx_Oracle.connect("e1125028","AAAaaa111","delta/decinfo.edu")
			return oracle_connection
		except cx_Oracle.DatabaseError as e:
            # Log error as appropriate
			return None
		


	lectureDonnee = staticmethod(lectureDonnee)
	insertionMaps = staticmethod(insertionMaps)
	testConnection = staticmethod(testConnection)



#DAO.lectureDonnee()


		
