## -*- coding: utf-8 -*-
class DTO(object):
	maps = []
	numeroMap = None

class MAP():
	def __init__(self):
		self.idMaps = None
		self.posX1 = None
		self.posY1 = None
		self.posX2 = None
		self.posY2 = None
		self.largeurMap = None
		self.hauteurMap =None
		self.case = []
		self.nomNiveau = "default"
		self.date = None
		self.status = ""
		self.delaisMinApp = None
		self.delaisMaxApp = None

class Case():
	def __init__(self):
		self.typeCase = None
		self.posXCase = None
		self.posYCase = None
		self.arbre = False