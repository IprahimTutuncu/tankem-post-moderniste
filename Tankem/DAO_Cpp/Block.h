#ifndef BLOCK_H
#define BLOCK_H

enum Block_type {PLANCHER=0,MUR_FIXE=1,MUR_VERTICAL=2,MUR_VERTICAL_INVERSE=3};

class Block {
public:

	Block(){
		type = Block_type::PLANCHER;
		arbre = false;
	}
	
	Block(Block_type bt,bool a){
		type = bt;
		arbre = a;
	}

	Block_type getType(){
		return type;
	}

	bool getArbre(){
		return arbre;
	}

	void setBlock(Block_type bt,bool a){
		type = bt;
		arbre = a;
	}

private:
	Block_type type;
	bool arbre;
};
#endif