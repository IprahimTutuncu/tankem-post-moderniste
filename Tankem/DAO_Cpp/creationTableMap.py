import cx_Oracle

def connection():
	oracle_connection = cx_Oracle.connect("e1125028","AAAaaa111","delta/decinfo.edu")
	return oracle_connection

def creerTables():
	oracle_connection = connection()
	oracle_cursor = oracle_connection.cursor()

	table_status = """
		CREATE TABLE status_list(
		id	NUMBER GENERATED ALWAYS AS IDENTITY(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
		nom VARCHAR2(20) NOT NULL,
		
		UNIQUE (nom)

		)
	"""

	table_block = """
		CREATE TABLE block_type(
		id	NUMBER GENERATED ALWAYS AS IDENTITY(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
		nom VARCHAR2(30) NOT NULL,
		
		UNIQUE (nom)

		)
	"""

	table_map = """
		CREATE TABLE info_map(
		id	NUMBER GENERATED ALWAYS AS IDENTITY(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
		titre VARCHAR2(64) NOT NULL,
		date_creation DATE,
		status_id NUMBER NOT NULL,
		delais_apparition_min NUMBER,
		delais_apparition_max NUMBER,
		largeur NUMBER NOT NULL,
		hauteur NUMBER NOT NULL,
		pos_joueur1_x NUMBER NOT NULL,
		pos_joueur1_y NUMBER NOT NULL,
		pos_joueur2_x NUMBER NOT NULL,
		pos_joueur2_y NUMBER NOT NULL,

		FOREIGN KEY (status_id) REFERENCES status_list(id),
		UNIQUE (titre)

		)
	"""

	table_liaison = """
		CREATE TABLE liaison_block_map(
		id	NUMBER GENERATED ALWAYS AS IDENTITY(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
		map_id NUMBER NOT NULL,
		block_id NUMBER NOT NULL,
		pos_x NUMBER NOT NULL,
		pos_y NUMBER NOT NULL,
		arbre VARCHAR2(10),

		FOREIGN KEY (map_id) REFERENCES info_map(id),
		FOREIGN KEY (block_id) REFERENCES block_type(id),
		CONSTRAINT bool_arbre CHECK (arbre = 'TRUE' OR arbre = 'FALSE'),
		CONSTRAINT uq_liaison UNIQUE (pos_x,pos_y,map_id)
		
		)
	"""
	print("Creation tables")
	print("------------------")

	oracle_cursor.execute(table_status)
	print("Creation Table status")
	
	oracle_cursor.execute(table_block)
	print("Creation table type block")
	

	oracle_cursor.execute(table_map)
	print("Creation table info map")
	
	oracle_cursor.execute(table_liaison)
	print("Creation table liaison map-block")
	
	print("")
	
	print("Ajout valeurs status")
	print("---------------------------")
	oracle_cursor.execute("INSERT INTO status_list(nom) VALUES('Actif')")
	print("Ajout status actif")
	oracle_cursor.execute("INSERT INTO status_list(nom) VALUES('Test')")
	print("Ajout status test")
	oracle_cursor.execute("INSERT INTO status_list(nom) VALUES('Inactif')")
	print("Ajout status inactif")

	print("")
	
	print("Ajout valeurs type block")
	print("------------------------------")
	oracle_cursor.execute("INSERT INTO block_type(nom) VALUES('Plancher')")
	print("Ajout type plancher")
	oracle_cursor.execute("INSERT INTO block_type(nom) VALUES('Mur Fixe')")
	print("Ajout type mur fixe")
	oracle_cursor.execute("INSERT INTO block_type(nom) VALUES('Mur Vertical')")
	print("Ajout type mur mouvement vertical")
	oracle_cursor.execute("INSERT INTO block_type(nom) VALUES('Mur Vertical Inverse')")
	print("Ajout type mur mouvement vertical inverse")


	oracle_connection.commit();
	oracle_connection.close();

def effaceTable():

	oracle_connection = connection()
	oracle_cursor = oracle_connection.cursor()
	
	print("Effacer les tables")
	print("----------------------")
	oracle_cursor.execute("DROP TABLE liaison_block_map")
	print("Table liaison map-block effacee")
	oracle_cursor.execute("DROP TABLE info_map")
	print("Table info map effacee")
	oracle_cursor.execute("DROP TABLE block_type")
	print("Table type block effacee")
	oracle_cursor.execute("DROP TABLE status_list")
	print("Table status effacee")

	oracle_connection.commit();
	oracle_connection.close();
	
	
def creerTrigger():
	oracle_connection = connection()
	oracle_cursor = oracle_connection.cursor()
	
	trigger_info_map = """
		CREATE OR REPLACE TRIGGER trigger_info_map
		BEFORE UPDATE OR INSERT ON info_map 
		FOR EACH ROW
		DECLARE
			buff NUMBER;
		BEGIN
			IF :new.delais_apparition_min > :new.delais_apparition_max THEN
				buff := :new.delais_apparition_min;
				:new.delais_apparition_min  := :new.delais_apparition_max;
				:new.delais_apparition_max := buff;
			END IF;
			
			/** TRIGGER POS MAX **/
			
			IF :new.pos_joueur1_x >= :new.largeur THEN
				:new.pos_joueur1_x := :new.largeur-1;
			END IF;
			
			IF :new.pos_joueur1_y >= :new.hauteur THEN
				:new.pos_joueur1_y := :new.hauteur-1;
			END IF;
			
			IF :new.pos_joueur2_x >= :new.largeur THEN
				:new.pos_joueur2_x := :new.largeur-1;
			END IF;
			
			IF :new.pos_joueur2_y >= :new.hauteur THEN
				:new.pos_joueur2_y := :new.hauteur-1;
			END IF;
			
			/** TRIGGER POS MIN **/
			
			IF :new.pos_joueur1_x < 0 THEN
				:new.pos_joueur1_x := 0;
			END IF;
			
			IF :new.pos_joueur1_y < 0 THEN
				:new.pos_joueur1_y := 0;
			END IF;
			
			IF :new.pos_joueur2_x < 0 THEN
				:new.pos_joueur2_x := 0;
			END IF;
			
			IF :new.pos_joueur2_y < 0 THEN
				:new.pos_joueur2_y := 0;
			END IF;
			
		END;
			
		"""
		
	trigger_liaison_block_map = """
		CREATE OR REPLACE TRIGGER trigger_liaison_block_map
		BEFORE UPDATE OR INSERT ON liaison_block_map 
		FOR EACH ROW
		DECLARE
			var_l NUMBER;
			var_h NUMBER;
		BEGIN
			SELECT largeur,hauteur INTO var_l,var_h FROM info_map WHERE id = :new.map_id;
			
			/** TRIGGER POS X **/
			
			if :new.pos_x >= var_l THEN
				:new.pos_x := var_l-1;
			END IF;
			
			if :new.pos_x < 0 THEN
				:new.pos_x := 0;
			END IF;
			
			/** TRIGGER POS Y **/
			
			if :new.pos_y >= var_h THEN
				:new.pos_y := var_h-1;
			END IF;
			
			if :new.pos_y < 0 THEN
				:new.pos_y := 0;
			END IF;
			
		END;
	"""
	
	print("Creation triggers")
	print("--------------------")
		
	oracle_cursor.execute(trigger_info_map)
	print("Creation trigger sur info_map")
	oracle_cursor.execute(trigger_liaison_block_map)
	print("Creation trigger sur liaison_block_map")
		
	oracle_connection.commit();
	oracle_connection.close();
	
def defaultMap():
	map1 = """INSERT INTO info_map(titre,date_creation,status_id,delais_apparition_min,delais_apparition_max,largeur,hauteur,pos_joueur1_x,pos_joueur1_y,pos_joueur2_x,pos_joueur2_y) 
		VALUES('DEFAUT1',TO_DATE('2018-04-06', 'YYYY-MM-DD'),(SELECT id FROM status_list WHERE nom = 'Actif'),5,10,6,6,0,0,5,5)"""
	
	map2 = """INSERT INTO info_map(titre,date_creation,status_id,delais_apparition_min,delais_apparition_max,largeur,hauteur,pos_joueur1_x,pos_joueur1_y,pos_joueur2_x,pos_joueur2_y) 
		VALUES('DEFAUT2',TO_DATE('2018-04-06', 'YYYY-MM-DD'),(SELECT id FROM status_list WHERE nom = 'Actif'),2,5,8,7,0,5,8,0)"""
	
	map3 = """INSERT INTO info_map(titre,date_creation,status_id,delais_apparition_min,delais_apparition_max,largeur,hauteur,pos_joueur1_x,pos_joueur1_y,pos_joueur2_x,pos_joueur2_y) 
		VALUES('DEFAUT3',TO_DATE('2018-04-06', 'YYYY-MM-DD'),(SELECT id FROM status_list WHERE nom = 'Actif'),15,40,9,12,2,4,9,10)"""
	
	print("Creation cartes par defaut")
	print("------------------------")
		
	oracle_connection = connection()
	oracle_cursor = oracle_connection.cursor()
	


	#creation map 1
	oracle_cursor.execute(map1)
	print("Creation map defaut 1")
		
	#Creation map 2
	oracle_cursor.execute(map2)
	for i in range(0,8):
		for j in range(0,7):
			block_type = "";
			arbre = ""
			if(i == j):
				block_type = "Mur Vertical Inverse";
			
			else:
				block_type = "Plancher";
			
			
			if (i == 0 or i == 7) and (j == 0 or j == 6):
				arbre = "TRUE";
			
			else:
				arbre = "FALSE";
			
		
			block = """INSERT INTO liaison_block_map(map_id,block_id,pos_x,pos_y,arbre) 
			VALUES((SELECT id FROM info_map WHERE titre = 'DEFAUT2'),(SELECT ID FROM block_type WHERE nom = '"""+block_type+"'),"+str(i)+","+str(j)+",'"+arbre+"')"
			
			if block_type != "Plancher" or arbre == "TRUE":
				oracle_cursor.execute(block)
			
	print("Creation map defaut 2")
		
	#creation map 3
	oracle_cursor.execute(map3)
	for i in range(0,9):
		for j in range(0,12):
			block_type = "Plancher";
			if i > 4 and j < 6:
				block_type = "Mur Fixe"
			else:
				if i <= 4 and j >= 6:
					block_type = "Mur Fixe"
			
			arbre = "FALSE"
			
			if i % 5 == 0 and j % 5 == 0:
				arbre = "TRUE"
		
			block = """INSERT INTO liaison_block_map(map_id,block_id,pos_x,pos_y,arbre) 
			VALUES((SELECT id FROM info_map WHERE titre = 'DEFAUT3'),(SELECT ID FROM block_type WHERE nom = '"""+block_type+"'),"+str(i)+","+str(j)+",'"+arbre+"')"
			
			if block_type != "Plancher" or arbre == "TRUE":
				oracle_cursor.execute(block)
	print("Creation map defaut 3")
		
	oracle_connection.commit();
	oracle_connection.close();
		
	
if __name__ == "__main__":
	print("Creation tables Etape 2")
	print("-------------------------")
	print("")
	effaceTable()
	print("--------------------")
	print("")
	creerTables()
	print("--------------------")
	print("")
	creerTrigger()
	print("--------------------")
	print("")
	defaultMap()

