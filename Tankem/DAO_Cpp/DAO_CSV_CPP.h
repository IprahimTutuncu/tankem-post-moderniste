
#ifndef DAO_CSV_CPP_H
#define DAO_CSV_CPP_H

#include "DTO_CPP.h"
#include <iostream>
#include <fstream>  
#include <stdio.h>

class DAO_CSV{
public:
	static std::string write(DTO dto){
		const std::string nom = "mapInfo.csv";
		const std::string nomOutput = "mapOutputMessage.txt";
		std::ofstream outfile(nom);

		std::string status;
		std::string result;
		switch (dto.status) {
		case 0:
			status = "Actif";
			break;
		case 1:
			status = "Inactif";
			break;
		case 2:
			status = "Test";
		}

		outfile << dto.nom << ",";
		outfile << status << ",";
		outfile << dto.delais_apparition_min << ",";
		outfile << dto.delais_apparition_max << ",";
		outfile << dto.largeur << ",";
		outfile << dto.hauteur << ",";
		outfile << dto.pos_joueur1_x << ",";
		outfile << dto.pos_joueur1_y << ",";
		outfile << dto.pos_joueur2_x << ",";
		outfile << dto.pos_joueur2_y << ",";
		outfile << dto.date;

		outfile << std::endl;

		for (int i = 0; i < dto.hauteur; i++) {
			for (int j = 0; j < dto.largeur; j++) {
				Block b = dto.terrain[i][j];

				std::string type;

				switch (b.getType()) {
				case 0:
					type = "Plancher";
					break;
				case 1:
					type = "Mur Fixe";
					break;
				case 2:
					type = "Mur Vertical";
					break;
				case 3:
					type = "Mur Vertical Inverse";
				}



				if(b.getType() != Block_type::PLANCHER || b.getArbre()){
					std::string arbre;

					if (b.getArbre()) {
						arbre = "TRUE";
					}
					else {
						arbre = "FALSE";
					}

					outfile << j << ",";
					outfile << i << ",";
					outfile << type << ",";
					outfile << arbre;
					outfile << std::endl;
				}
			}
		}


		outfile.close();

		DAO_CSV::runPython();

		/*
		std::ifstream iffile(nom);

		if (iffile) {
			iffile.close();
			std::cout << "Erreur!" << std::endl;
			remove(nom.c_str());

		}
		else {
			std::cout << "Envoie r�ussi" << std::endl;
		}
		*/
		std::ifstream iffile(nomOutput);
		if (iffile) {
			/*Get Result of Python script and remove the text file*/
			const int bufferSize = 80;
			std::string output;
			char buffer[bufferSize];
			iffile.getline(buffer, bufferSize);
			std::cout << buffer << std::endl;
			result = buffer;
			iffile.close();
			remove(nomOutput.c_str());
		}
		else {
			std::cout << "Aucun output" << std::endl;
		}
		return result;
	}

private:
	static void runPython() {
		system("C:/LogicielsPortables/Panda3D-1.9.1-x64/python/python test.py");

	}

};

#endif