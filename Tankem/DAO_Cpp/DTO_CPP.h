#ifndef DTO_CPP_H
#define DTO_CPP_H

#include "Block.h"
#include <string>

enum Status {ACTIF=0,INACTIF=1,TEST=2};

class DTO{
public:
	DTO() {
		DTO("TEST",12,12,Status::TEST);
	}
	DTO(std::string titre,int width, int height, Status state) {
		nom = titre;
		date = "2018-04-13";
		status = state;
		delais_apparition_max = 10;
		delais_apparition_min = 10;
		largeur = width;
		hauteur = height;
		pos_joueur1_x = 0;
		pos_joueur1_y = 0;
		pos_joueur2_x = 5;
		pos_joueur2_y = 5;

		terrain = new Block*[hauteur];
		for (int i = 0; i < hauteur; i++) {
			terrain[i] = new Block[largeur];
		}
	}

	std::string nom;
	std::string date;
	Status status;
	int delais_apparition_min;
	int delais_apparition_max;
	int largeur;
	int hauteur;
	int pos_joueur1_x;
	int pos_joueur1_y;
	int pos_joueur2_x;
	int pos_joueur2_y;
	Block** terrain;
};
#endif