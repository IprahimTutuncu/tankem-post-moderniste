
#include <string>
#include <conio.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#include "DAO_CSV_CPP.h"

void randomMap(std::string titre);

int main(){
	srand(time(NULL));

	for (int i = 0; i < 10; i++) {
		std::string titre = "RANDOM " + i;
		randomMap(titre);
	}

	return 0;
}

void randomMap(std::string titre) {

	DTO dto(titre,10, 10, Status::ACTIF);

	for (int i = 0; i < dto.hauteur; i++) {
		for (int j = 0; j < dto.largeur; j++) {
			int r = rand() % 4;
			Block_type block_type;

			switch (r) {
			case 0:
				block_type = Block_type::PLANCHER;
				break;
			case 1:
				block_type = Block_type::MUR_VERTICAL;
				break;
			case 2:
				block_type = Block_type::MUR_VERTICAL_INVERSE;
				break;
			case 3:
				block_type = Block_type::MUR_FIXE;
			}

			bool arbre;

			r = rand() % 2;

			if (r == 0) {
				arbre = true;
			}
			else {
				arbre = false;
			}

			dto.terrain[i][j] = Block(block_type, arbre);
		}

	}

	DAO_CSV::write(dto);


	_getch();
}
