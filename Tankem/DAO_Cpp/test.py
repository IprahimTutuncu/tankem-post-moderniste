import cx_Oracle
import os
import csv
from DTO import *
from DAO import *
import datetime

if __name__ == "__main__":
	nom = "mapInfo.csv"
	
	with open(nom,"rb") as file:
		reader = csv.reader(file,"excel")
		i = 0;
			
		maps = MAP()
		
		for row in reader:
			if i == 0:
				maps.posX1 = row[6]
				maps.posY1 = row[7]
				maps.posX2 = row[8]
				maps.posY2 = row[9]
				maps.largeurMap = row[4]
				maps.hauteurMap = row[5]
				maps.nomNiveau = row[0]
				maps.date = row[10]
				maps.status = row[1]
				maps.delaisMinApp = row[2]
				maps.delaisMaxApp = row[3]

			else:
				case = Case()
				
				case.typeCase = row[2]
				case.posXCase = row[0]
				case.posYCase = row[1]
				case.arbre = row[3]
				
				maps.case.append(case)

			
			i+=1
		DTO.maps.append(maps)
		
	try:
		DAO.insertionMaps()
	
		os.remove(nom)
	except:
		pass
	
	